/**
 * Created by AgungPramono on 30/03/2017.
 */

var button = document.querySelector('#greet');
button.addEventListener('click', function () {
    var name = document.querySelector('#name').value;
    alert('Hello,' + name);
});

// JavaScript New Syntax
/*
 var button = document.querySelector('#greet');
 button.addEventListener('click', () => {var name = document.querySelector('#name').value;
 alert('Hello,' + name);
 })
 ;
 */
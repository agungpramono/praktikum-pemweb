/**
 * Created by AgungPramono on 30/03/2017.
 */
var button = document.querySelector('#request');
button.addEventListener('click', function () {
    var xhr = new XMLHttpRequest();
    var url = 'http://localhost/Kelas PW/Latihan PemWeb/JS & AJAX/ajax-basic.php';
	//Kelas PW\Latihan PemWeb\JS & AJAX
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            alert(xhr.responseText);
        }
    };
    xhr.send(null)
});

// JavaScript New Syntax
/*
 var button = document.querySelector('#request');
 button.addEventListener('click',() => {
 var xhr = new XMLHttpRequest();
 var url = 'http://localhost/Latihan PemWeb/JS & AJAX/ajax-basic.php';
 xhr.open('GET', url, true);
 xhr.onreadystatechange = () =>{
 if (xhr.readyState === 4 && xhr.status === 200) {
 alert(xhr.responseText);
 }
 };
 xhr.send(null)
 });
 */
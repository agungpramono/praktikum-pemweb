<?php

class Contact extends Controller
{
    public function read()
    {
        $query = $this->loadModel('ContactModel')->getAll();
        $data['query'] = $query;
        $this->loadView('read', $data);
    }

    public function delete()
    {
        $id = $_GET['id'];
        $this->loadModel('ContactModel')->remove($id);
        header('location: index.php?controller=Contact&function=read');
    }
}
<?php

class Home extends Controller{

    public function index(){
        //load model
        $homeModel = $this->loadModel('HomeModel');

        //Get data from the model
        $fromDatabase = $homeModel->getData();

        //inject variable to view
        $data['fromDB'] = $fromDatabase;

        //load view
        $this->loadView('home',$data);
    }
}
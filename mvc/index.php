<?php
/**
 * Created by PhpStorm.
 * User: Agung
 * Date: 04/05/2017
 * Time: 8:57
 */

//disable error yang sifatnya ringan (Cuma notice), active error_reporting syntax bellow
//error_reporting(~E_NOTICE);

// 'Home' is the default controller
$controller = $_GET['controller'] ? $_GET['controller'] : 'Home';

// 'index' is the default controller
$function = $_GET['function'] ? $_GET['function'] : 'index';

include_once "controllers/Controller.class.php";
include_once "controllers/$controller.class.php";

//Go!
$con = new $controller;
$con->$function();
<?php
if (!isset($_COOKIE['name'])) {
    header('Location: form.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Chat</title>
    <style>
        #chat {
            width: 300px;
            height: 400px;
            border: 1px solid #555;
        }

        #message {
            width: 300px;
        }
    </style>
</head>
<body>
<div id="chat"></div>
<input type="text" id="message">
<script src="chat.js"></script>
</body>
</html>

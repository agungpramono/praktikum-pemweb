/**
 * Created by AgungPramono on 06/04/2017.
 */
var chat = document.querySelector('#chat');
var message = document.querySelector('#message');

(function loadChat() {
    var xhr = new XMLHttpRequest();
    var url = 'http://localhost/state/chat-read.php';

    xhr.open('GET', url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            chat.innerHTML = xhr.responseText;
        }
    };
    xhr.send(null);
    setTimeout(loadChat, 1000);
})();

message.addEventListener('keyup', function (event) {
    if (event.keyCode == 13 && message.value.length) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost/state/chat-write.php';
        var params = 'message=' +message.value;

        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.send(params);
        message.value = '';
    }
});
<?php
$mysqli = new mysqli('localhost', 'root', 'mysql', 'pemwebH');

if ($mysqli->connect_errno) {
    die('database connection error');
}

// Mengambil id dari url
$id = $_GET['id'];
$sql = "select * from contact where id = $id";

// Eksekusi sql
$query = $mysqli->query($sql);

// Mengambil baris
$row = $query->fetch_object();

?>
<style>
    input {
        margin: 3px;
    }
</style>
<h3>Insert Data</h3>
<form action="edit_process.php" method="post">
    <input type="hidden" name="id" value="<?php echo $row->fid; ?>">
    <label>
        <input type="text" name="first_name" value="<?php echo $row->first_name; ?>" placeholder="First Name"
               maxlength="50" autofocus>
    </label>
    <br>
    <label>
        <input type="text" name="last_name" value="<?php echo $row->last_name; ?>" placeholder="Last Name" maxlength="50">
    </label>
    <br>
    <label>
        <input type="text" name="address" value="<?php echo $row->address; ?>" placeholder="Address" maxlength="50">
    </label>
    <br>
    <input type="submit" value="Save">
</form>
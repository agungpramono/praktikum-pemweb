/**
 * Created by AgungPramono on 30/03/2017.
 */
var username = document.querySelector('#username');
var message = document.querySelector('#message');
username.addEventListener('blur', function () {
    if (username.value == '') {
        return;
    }
    message.innerHTML = 'Checking';

    var xhr = new XMLHttpRequest();
    var url = 'http://localhost/Kelas PW/TugasPemWeb/ajax-form.php?username='+username.value;
	//Kelas PW\Latihan PemWeb\JS & AJAX
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200){
            message.innerHTML = xhr.responseText;
        }
    }
    xhr.send(null);
});